//
//  main.m
//  ObjectiveCPerformanceTest
//
//  Created by Philipp Hunger on 13.04.15.
//  Copyright (c) 2015 Philipp Hunger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PerformanceTest:NSObject
+ (int)fib:(int) n;
+ (void) deserializeJSON:(NSData*) data;
@end

@implementation PerformanceTest

+ (int)fib:(int)n {
    if (n == 0) {
        return 0;
    } else if (n == 1) {
        return 1;
    } else {
        return [PerformanceTest fib:n - 1] + [PerformanceTest fib:n - 2];
    }
}

+ (void) deserializeJSON:(NSData*)data {
    [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error:nil];
}

@end

static const int kNumber = 40;
static const int kMaxIterations = 100;

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSDate *date;
        double totalElapsedTime = 0;
        double averageElapsedTime;
        int result;
        printf("Fibonacci-Test\n");
        for (int i = 1; i <= kMaxIterations; i++) {
            date = [NSDate date];
            result = [PerformanceTest fib:kNumber];
            totalElapsedTime += round([date timeIntervalSinceNow] * -1000.0);
        }
        averageElapsedTime = totalElapsedTime / kMaxIterations;
        printf("\tfib(%d) in %g ms\n", kNumber, averageElapsedTime);
        
        printf("JSON-Test\n");
        totalElapsedTime = 0;
        date = [NSDate date];
        NSData* data = [NSData dataWithContentsOfFile:@"/Users/philipphunger/testdata.json"];
        printf("\tFile red in %g ms\n", round([date timeIntervalSinceNow] * -1000.0));
        date = [NSDate date];
        for (int i = 1; i <= kMaxIterations; i++) {
            [PerformanceTest deserializeJSON:data];
            totalElapsedTime += round([date timeIntervalSinceNow] * -1000.0);
        }
        averageElapsedTime = totalElapsedTime / kMaxIterations;
        printf("Json data deserialized in %g ms\n", averageElapsedTime);
    }
    return 0;
}